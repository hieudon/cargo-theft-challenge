
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hieud
 */
public class Main extends JPanel implements ActionListener {

    private ImageIcon imgBackground;
    private ImageIcon imgTruck = new ImageIcon("truck.png");
    private ImageIcon imgDriver = new ImageIcon("driver.png");
    private int x,y,x2,y2,index,index2;
    private double currentx,currentx2,currenty,currenty2,lastx,lastx2,lasty,lasty2;
    private Timer everySec;
    private String a;
    private boolean valid=true;
    private DecimalFormat ft = new DecimalFormat(".####");
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Main();
     
        
        // TODO code application logic here
    }
    public Main(){
        
        
        //}
        imgBackground = new ImageIcon("grid.png");

	setLayout(null);
	setFocusable(true);

        getCoordinate();
        x=1200;
        y=700;
        x2=1200;
        y2=700;
        everySec = new Timer(50, this);
        
	JFrame frame = new JFrame();
        
	frame.setContentPane(this);
	frame.setSize(imgBackground.getIconWidth(), imgBackground.getIconHeight());
	frame.setLocationRelativeTo(null);
	frame.setTitle("stopWATCH");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setResizable(true);
        frame.setUndecorated(true);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
	frame.setVisible(true);
        
        everySec.start();
        
        
    }
    
    public void paintComponent(Graphics g) {

	super.paintComponent(g);
	Graphics2D g2 = (Graphics2D) g;
	Font f = new Font("Dialog", Font.PLAIN, 20);
        g2.setFont(f);
        
        g2.drawImage(imgBackground.getImage(), 0, 0, 3000, 1800, this);
        g2.drawImage(imgTruck.getImage(), x2, y2, this);
        g2.drawImage(imgDriver.getImage(), x,y, this);
        g2.setColor(Color.BLACK);
        g2.fillRect(x+43, y-20, 175, 25);
        g2.fillRect(x2+43, y2-20, 175, 25);
        g2.setColor(Color.GREEN);
        g2.drawString(ft.format(currentx)+","+ft.format(currenty), x+50, y);
        g2.drawString(ft.format(currentx2)+","+ft.format(currenty2), x2+50, y2);
        g2.drawString("DRIVER:     "+currentx+","+currenty, 1800, 30);
        g2.drawString("TRUCK:       "+currentx2+","+currenty2, 1800, 60);
        
        //47-50
        //-122 -124 longtitude
   
	}
 
    
    public void actionPerformed(ActionEvent e) {
            
//            currentx += 1;
//            currenty2 += 1;
            getCoordinate();
            repaint();
            
    }
    
    void getCoordinate ()
{ 
  try
  {
    
    URL                url,url2; 
    URLConnection      urlConn,urlConn2; 
    BufferedReader     dis,dis2;
    
    url = new URL("https://sigma-myth-229819.appspot.com/driverGps");
    urlConn = url.openConnection(); 
    urlConn.setDoInput(true); 
    urlConn.setUseCaches(false);

    url2 = new URL("https://sigma-myth-229819.appspot.com/truckGps");
    urlConn2 = url2.openConnection(); 
    urlConn2.setDoInput(true); 
    urlConn2.setUseCaches(false);
    
    
    dis = new BufferedReader(new InputStreamReader(urlConn.getInputStream())); 
    String driverCoor = dis.readLine();
    
    dis2 = new BufferedReader(new InputStreamReader(urlConn2.getInputStream()));
    String truckCoor = dis2.readLine();
    
    
    //String b = "[{\"id\":\"400116389\",\"lat\":47,\"long\":90},{\"id\":\"1084068051\",\"lat\":null,\"long\":null},{\"id\":\"1084068100\",\"lat\":null,\"long\":null},{\"id\":\"1084068111\",\"lat\":49.157,\"long\":-124.019},{\"id\":\"1088214007\",\"lat\":49.1560554504395,\"long\":-123.01741027832},{\"id\":\"1088214013\",\"lat\":49.1560554504395,\"long\":-123.01741027832},{\"id\":\"400116r424389\",\"lat\":62,\"long\":115},{\"id\":\"1088214236\",\"lat\":null,\"long\":null},{\"id\":\"1088217018\",\"lat\":null,\"long\":null},{\"id\":\"400114326389\",\"lat\":80,\"long\":200}]";
    dis.close(); 
    dis2.close();
     
    String rawData = driverCoor.replaceAll("[a-z]|[\\\"]|[:]|[{]|[\\}]|[\\[]|[\\]]", "");
    String[] data = rawData.split("[\\,]");
    
    String rawData2 = truckCoor.replaceAll("[a-z]|[\\\"]|[:]|[{]|[\\}]|[\\[]|[\\]]", "");
    String[] data2 = rawData2.split("[\\,]");
    
    
    if(valid){
    for(int i=0;i<data.length;i++){
        if(data[i].compareTo("12345")==0){
            index = i;
            lastx = Double.valueOf(data[i+1]);
            lasty = Double.valueOf(data[i+2]);
            currentx = Double.valueOf(data[i+1]);
            currenty = Double.valueOf(data[i+2]);

        }
    }

    for(int j=0;j<data2.length;j++){
        if(data2[j].compareTo("1084067241")==0){
            index2 = j;
            lastx2 = Double.valueOf(data2[j+1]);
            lasty2 = Double.valueOf(data2[j+2]);
            currentx2 = Double.valueOf(data2[j+1]);
            currenty2 = Double.valueOf(data2[j+2]);
        }
    }
    valid = false;
    }
    //System.out.println(data2[index2+1]);
    
    currentx = Double.valueOf(data[index+1]);
    currenty = Double.valueOf(data[index+2]);
    currentx2 = Double.valueOf(data2[index2+1]);
    currenty2 = Double.valueOf(data2[index2+2]);
    
    x+= ( currentx - lastx)*500;
    y+= (currenty - lasty)*500;
    x2+= (currentx2 - lastx2)*500;
    y2+= (currenty2 - lasty2)*500;

    lastx=currentx;
    lasty=currenty;
    lastx2=currentx2;
    lasty2=currenty2;
  }
    
    catch (MalformedURLException mue) {} 
    catch (IOException ioe) {} 
  } 
}
